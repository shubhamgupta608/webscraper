const express = require('express');
const cors = require('cors');
const morgan = require('morgan');

const bodyParser = require('body-parser');
const errorHandler = require('./helpers/error-handler')
const router = require('./routes/route');
const app = express();

app.use(bodyParser.json());
app.use(morgan('dev'));
app.use(cors());


app.use('/api/scrap', router)
//global error handler
app.use(errorHandler)

const port = 4000;
//listen port
app.listen(port, (err) => {
    if (err) throw err;
    console.log(`Express Server running on port ${port}`)
});