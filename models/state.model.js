//Customer Model
const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const stateSchema = new Schema({
    name: {
        type: String,
    },
    party: {
        type: String,
    },
    state: {
        type: String,
    },
    status: {
        type: String,
    },
    constituency: {
        type: String,
    },
    image: {
        type: String,
    }


})
module.exports = mongoose.model('StateModel', stateSchema) 
