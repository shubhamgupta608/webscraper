const mongoose = require('../mongo/connection')
const StateModel = mongoose.StateModel;

//Saving the customer data in mongodb
async function create(userParam) {
    try {
        
            let stateDetail = new StateModel(userParam)

            // await stateDetail.save();
        
        console.log("Schema was created and data saved into the database.")
    }
    catch (err) {
        console.log(err)
    }
}


module.exports = {
    create,
};
