const express = require('express');
const router = express.Router();
const stateService = require('../services/state.service');
var request = require('request');
var cheerio = require('cheerio');
//Check route for Initiate
router.route('/').get((req, res) => {
    res.json({
        msg: "Initiated"
    })
})

//Insert Route for insert data to customer.txt to atlas 
router.route('/insertData').get(stateData);


//create and saving the data in atlas
function stateData(req, res, next) {
        console.log("Body is:",req.body)
        stateService.create(req.body)
            .then(() => res.status(200).json({ data: 'Added Successfully' }))
            .catch(err => next(err));
    
}

module.exports = router;